//
//  AppDelegate.h
//  ExampleGame
//
//  Created by Michael Panikovsky on 23/09/14.
//  Copyright (c) 2014 Michael Panikovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

